<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("���� ����������");
?>
<?
CModule::IncludeModule('iblock');

require('CAutoWizard.php');

use \Bitrix\Main\Page\Asset,
	\Bitrix\Main\Web\Json;

Asset::getInstance()->addCss($APPLICATION->GetCurPage().'css/style.css');
Asset::getInstance()->addJs($APPLICATION->GetCurPage().'js/script.js');

// ���������� ������ $arConfigs �� ����� ������������ �����������
if(file_exists('UserConfig.php')) {
	include_once('UserConfig.php');
}

?>

<?
$arDefaultConfig = array(
	'partnerID' => 'aspro',
	'solutionID' => 'next',
	'moduleClass' => 'CNext',
	'cacheClass' => 'CNextCache',
	'exportPath' => '/bitrix/wizards_auto/',
	'exportSITE_ID' => (SITE_ID ? SITE_ID : 's1'),
	'includeModules' => array('iblock'),
	'IBlockIDs' => array(),
);

$arConfigs = $arConfigs ? $arConfigs : array();

$arConfig = array_merge($arDefaultConfig, $arConfigs);

if($_POST['AUTO_WIZ_LAST_ACTIONS']) {
	$APPLICATION->RestartBuffer();
	$arConfig['LAST_ACTIONS'] = true;
	$AutoWizard = new CAutoWizard($arConfig);
	print_r($AutoWizard->GenerateFullWizard());
	die();
}
?>

<?

// ���������� ������, ���������� � ������
$arModules = array(
	'iblock',
	'sale',
);

// �������� ������ ����� � ������ � ����� �����
$arRootFiles = scandir($_SERVER['DOCUMENT_ROOT']);
foreach ($arRootFiles as $file) {
	if(!in_array($file, array('.', '..', 'bitrix', 'upload', basename(__DIR__)))){
		if(is_dir($_SERVER['DOCUMENT_ROOT'].'/'.$file)){
			$arPublicDirs[] = $file;
		} else {
			if(strpos($file, 'sitemap_') === false) {
				$arPublicFiles[] = $file;
			}
		}
	}
}

// ���������� ����� ������ �� ���������
$arFileMasks = array(
	'*.back*',
	'_*',
	'*_',
);

// �������� ������ ����������
$resIblocks = CIBlock::GetList(array("NAME"=>"ASC"), array(), false);

while($iblock = $resIblocks->Fetch()) {
	$arIblocks[] = array(
		'NAME' => $iblock['NAME'],
		'CODE' => $iblock['CODE'],
		'ID' => $iblock['ID'],
		'ACTIVE' => $iblock['ACTIVE'],
	);
}
?>

<div class="formTitle"><p>��������� ���� �����������</p></div>
<div id="stepInformer" style="display: none;margin: 10px 0;"><span class="stepName"></span><span class="stepInfo"></span></div>

<div id="formAutoWizard">

	<div class="stepTitle">�������� ���������</div>
	<div class="stepWrapper active" data-step="main">
		
		<div class="subStep">
			<input type="text" class="stepSettings" name="partnerID" value="<?=$arConfig['partnerID'] ? $arConfig['partnerID'] : ''?>" placeholder="��� ��������">
			<input type="text" class="stepSettings" name="solutionID" value="<?=$arConfig['solutionID'] ? $arConfig['solutionID'] : ''?>" placeholder="��� �������">
			<input type="text" class="stepSettings" name="moduleClass" value="<?=$arConfig['moduleClass'] ? $arConfig['moduleClass'] : ''?>" placeholder="����� ������">
			<input type="text" class="stepSettings" name="cacheClass" value="<?=$arConfig['cacheClass'] ? $arConfig['cacheClass'] : ''?>" placeholder="����� ����">
			<input type="text" class="stepSettings" name="templateDir" value="<?=$arConfig['templateDir'] ? $arConfig['templateDir'] : ''?>" placeholder="����� � �������� �����">
			<input type="text" class="stepSettings" name="exportPath" value="<?=$arConfig['exportPath'] ? $arConfig['exportPath'] : '/bitrix/wizards_auto/'?>" placeholder="���� � ������">
			<input type="text" class="stepSettings" name="exportSITE_ID" value="<?=$arConfig['exportSITE_ID'] ? $arConfig['exportSITE_ID'] : SITE_ID?>" placeholder="ID �����">
		</div>

		<div class="subStep">
			<div class="subTitle">���������� ������</div>
			<?if($arModules):?>
				<div class="scrollForm">
					<?foreach ($arModules as $module):?>
						<?$checked = (in_array($module, $arConfig['includeModules']) ? 'checked' : '')?>
						<div class="checkboxWrapper">
							<input type="checkbox" id="<?=$module?>" name="includeModules[]" value="<?=$module?>" <?=$checked?> >
							<label for="<?=$module?>"><?=$module?></label>
						</div>
					<?endforeach;?>
				</div>
			<?endif;?>
		</div>

	</div>

	<div class="stepTitle">��������� ��������� �����</div>
	<div class="stepWrapper" data-step="public">

		<div class="subStep">
			<div class="subTitle">����� ��������� �����, ������� ������ � ������</div>
			<?if($arPublicDirs):?>
				<div class="scrollForm">
					<?foreach ($arPublicDirs as $dir):?>
						<?
							if($arConfigs) {
								$checked = (in_array($dir, $arConfigs['PUBLIC']['DIRS']) ? 'checked' : '');
							} else {
								$checked = (strpos($dir, 'site_') !== 0 ? 'checked' : '');
							}
						?>
						<div class="checkboxWrapper">
							<input type="checkbox" id="<?=$dir?>" name="PUBLIC[DIRS][]" value="<?=$dir?>" <?=$checked?> >
							<label for="<?=$dir?>">
								<?=$dir?>	
							</label>
						</div>
					<?endforeach;?>
				</div>
			<?endif;?>
		</div>

		<div class="subStep">
			<div class="subTitle">����� ������, ������� �� ������ � ������</div>
			<? $arFileMasks = ($arConfig['FileMasks'] ? $arConfig['FileMasks'] : $arFileMasks);
				if($arFileMasks) {
				foreach ($arFileMasks as $mask):?>
					<input type="text" name="FileMasks[]" value="<?=$mask?>">
				<?endforeach;
			}?>
		</div>

		<div class="subStep">
			<div class="subTitle">����� ��������� �����, ������� ������ � ������</div>
			<?if($arPublicFiles):?>
				<div class="scrollForm">
					<?foreach ($arPublicFiles as $file):?>
						<?
							if($arConfig) {
								$checked = (in_array($file, $arConfig['PUBLIC']['FILES']) ? 'checked' : '');
							} else {
								$checked = (strpos($file, 'site_') !== 0 ? 'checked' : '');
							}
						?>
						<div class="checkboxWrapper">
							<input type="checkbox" id="<?=$file?>" name="PUBLIC[FILES][]" value="<?=$file?>" <?=$checked?> >
							<label for="<?=$file?>">
								<?=$file?>	
							</label>
						</div>
					<?endforeach;?>
				</div>
			<?endif;?>
		</div>

	</div>

	<div class="stepTitle">��������� ����������</div>
	<div class="stepWrapper">

		<div class="subStep">
			<div class="subTitle">������ ���������� �����</div>
			<?if($arIblocks):?>
				<div class="scrollForm">
					<?foreach ($arIblocks as $iblock):?>
						<?
							if($arConfig) {
								$checked = (in_array($iblock['ID'], $arConfig['IBlockIDs']) ? 'checked' : '');
							} else {
								$checked = ($iblock['ACTIVE'] == 'Y' ? 'checked' : '');
							}
						?>
						<div class="checkboxWrapper">
							<input type="checkbox" id="<?=$iblock['CODE']?>" name="IBlockIDs[]" value="<?=$iblock['ID']?>" <?=$checked?> >
							<label for="<?=$iblock['CODE']?>">
								<?=$iblock['NAME'].' ('.$iblock['CODE'].')'?>	
							</label>
						</div>
					<?endforeach;?>
				</div>
			<?endif;?>
		</div>

	</div>

	<div class="navButtonsWrapper">
		<div class="btn btn-default prevStep disabled" disabled>���������� ���</div>
		<div class="btn btn-default nextStep">��������� ���</div>
	</div>

	<div id="submitAutoWizard" class="btn btn-default">������ ������</div>
</div>

<?
if($_POST['AUTO_WIZ_FIRST_ACTIONS']) {
	$APPLICATION->RestartBuffer();
	$arConfig['FIRST_ACTIONS'] = true;
	$AutoWizard = new CAutoWizard($arConfig);
	print_r($AutoWizard->GenerateFullWizard());
?>
<script>
	var arIblocksForExport = <?=CUtil::PhpToJSObject($AutoWizard->arIblocksForExport)?>;
	var arIblocksForExportKey = 0;
	StartExport(arIblocksForExport, arIblocksForExportKey);
</script>

<?if(!$AutoWizard->arIblocksForExport):?>
	<script>
			BX.ajax.post(
				window.location.href,
				{'AUTO_WIZ_LAST_ACTIONS': true},
				function(){}
			);
	</script>
<?endif;?>

<?}?>



<div id="exportContainer" style="display: none;"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>