<?
require('CAsproFile.php');
require('CAsproLinkCreater.php');

class CAutoWizard{

	public $bError = false;
	public $lastError = '';
	public $arIblocksForExport = array();

	private $_arIBlocks = array();
	private $_arProps = array();
	private $_arServicesIBlockLang = array();
	private $_arPropertiesHints = array();
	private $_arPropertiesHintsKeys = array();

	function __construct($arConfig = array()){
		$this->_InitConfig($arConfig);
	}

	function GenerateFullWizard(){

		$this->_InitModules();
		$this->_PrepareData();

		if ($this->arConfig['FIRST_ACTIONS']) {
			$this->_MakeWizardDirs();
			//$this->_LinksCreate(); // /services/iblock/links.php
			//$this->_PublicCreate(); // * ����������� � public
			//$this->_TemplateCreate(); // * ����������� � templates
			$this->_GenerateWizardServicesIBlockAllScripts(); // * ��� ������� ��������� ���� ������ �������
			$this->_GenerateWizardServicesIBlockLang(); // * lang ��� �������� ������� ����������
			$this->_ExportIblocks(); // * ��������� ��������� � xml
			// .services.php
			// /services/public.php (urlrewrite)
			// /services/iblock/types.php
		} elseif($this->arConfig['LAST_ACTIONS']) {
			$this->_SetMacrosIblocks(); // * � ������ xml ���������� ������� s1 & / ��������� �� POST ����� �������������� �����, ���� php ������������ ������ js
			$this->_LinksCreate(); // /services/iblock/links.php
			// �������� ��� �������� ���������� �� �������
			//$this->_SetMacrosPublic($this->arConfig['wizardSiteDir']);// �������� ��� ������ �� ������ #SITE_DIR#
			// �������� ��� �������� ��������� �� �������
		}

		return !$this->bError;

	}

	private function _InitConfig($arConfig = array()){
		$this->arConfig = $arConfig;
		$this->arConfig['moduleID'] = $this->arConfig['partnerID'].'.'.$this->arConfig['solutionID'];
		//$this->arConfig['includeModules'][] = $this->arConfig['moduleID'];
		$this->arConfig['exportPath'] = str_replace('//', '/', '/'.$this->arConfig['exportPath'].'/');
		$this->arConfig['wizardDir'] = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.$this->arConfig['exportPath'].'/'.$this->arConfig['partnerID'].'/'.$this->arConfig['solutionID'].'/');
		$this->arConfig['wizardSiteDir'] = str_replace('//', '/', $this->arConfig['wizardDir'].'site/public/ru/');
		$this->arConfig['wizardTemplateDir'] = str_replace('//', '/', $this->arConfig['wizardDir'].'site/templates/'.$this->arConfig['templateDir'].'/');
		$this->arConfig['wizardServicesDir'] = str_replace('//', '/', $this->arConfig['wizardDir'].'site/services/');
		$this->arConfig['wizardServicesIblockExportDir'] = $this->arConfig['exportPath'].$this->arConfig['partnerID'].'/'.$this->arConfig['solutionID'].'/site/services/iblock/xml/ru';
		$this->arConfig['moduleDir'] = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->arConfig['partnerID'].'.'.$this->arConfig['solutionID'].'/');
	}

	private function _InitModules(){
		foreach($this->arConfig['includeModules'] as $strModule){
			if(!CModule::IncludeModule($strModule)){
				return $this->_SetError('Can`t include module '.$strModule);
			}
		}

		global $USER;
		if(!$USER->IsAdmin()){
			return $this->_SetError('Need authorize by admin');
		}

		return true;
	}

	private function _MakeWizardDirs(){
		if($this->bError){
			return false;
		}

		$arDirs = array(
			$this->arConfig['wizardDir'],
			$this->arConfig['wizardDir'].'css/',
			$this->arConfig['wizardDir'].'images/ru/',
			$this->arConfig['wizardDir'].'images/en/',
			$this->arConfig['wizardDir'].'js/',
			$this->arConfig['wizardDir'].'lang/ru/site/services/',
			$this->arConfig['wizardSiteDir'],
			$this->arConfig['wizardTemplateDir'],
			$this->arConfig['wizardServicesDir'].'main/lang/ru/',
			$this->arConfig['wizardServicesDir'].'iblock/lang/ru/',
			$this->arConfig['wizardServicesDir'].'iblock/lang/en/',
			$this->arConfig['wizardServicesDir'].'iblock/xml/',
			$this->arConfig['wizardServicesDir'].'iblock/xml/ru/',
			$this->arConfig['wizardServicesDir'].'search/',
		);

		foreach($arDirs as $dir){
			if(!is_dir($dir)){
				if(!mkdir($dir, 0777, true)){
					return $this->_SetError('Can`t create directory '.$dir);
				}
			}
		}

		return true;
	}

	private function _PrepareData(){
		if($this->bError){
			return false;
		}

		$resIBlocks = CIBlock::GetList(array(),  array('SITE_ID' => $this->arConfig['exportSITE_ID']), true);
		while($arItem = $resIBlocks->Fetch()){
			if(in_array($arItem['ID'], $this->arConfig['IBlockIDs'])){
				$arItem['FIELDS'] = CIBlock::getFields($arItem['ID']);
				$this->arIBlocks[$arItem['ID']] = $arItem;
			}
		}

		foreach($this->arIBlocks as $IBLOCK_ID => $arIBlock){
			$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID));
			while($arItem = $dbProperty->Fetch()){
				$this->arProps[$arItem["ID"]] = $arItem;
				$value = str_replace('"', '\"', $arItem['HINT']);
				if(strlen($arItem['HINT'])){
					if(($key = array_search($value, $this->_arPropertiesHints)) === false){
						$key = 'WZD_PROPERTY_HINT_'.count($this->_arPropertiesHints);
						$this->_arPropertiesHints[$key] = $value;
					}
					$this->_arPropertiesHintsKeys[$IBLOCK_ID][$arItem['CODE']] = $key;
				}
			}
		}

		return true;
	}

	private function _LinksCreate(){
		
		$CAsproLinkCreater = new CAsproLinkCreater($this->arIBlocks, $this->arConfig['cacheClass'], $this->arConfig['moduleID']);
		$linksContent = $CAsproLinkCreater->create();

		$linksContent = iconv('utf-8', 'windows-1251', $linksContent);

		// print_r($linksContent);
		// die();

		file_put_contents($this->arConfig['wizardServicesDir'].'iblock/links.php', $linksContent);

		return true;
	}

	private function _SetMacrosIblocks() {
		$files = scandir($_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir']);

		if($files) {
			foreach ($files as $fileName) {				
				if(strpos($fileName, '.xml') !== false) {
					$content = file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName);
					$content = str_replace(array('_'.SITE_ID, '>/'), array('_#IN_XML_SITE_ID#', '>#IN_XML_SITE_DIR#'), $content);

					$pattern = '/(<����>[^\/]*?<����������>)(BASE)(<\/����������>.*?<\/����>).*?(\s*<\/����>)/is';
					$content = preg_replace($pattern, '${1}#IN_XML_PRICE_ID#${3}${4}', $content);

					$pattern = '/(<�������>[^\/]*?<��>)(BASE)(<\/��>.*?<\/�������>).*?(\s*<\/�������>)/is';
					$content = preg_replace($pattern, '${1}#IN_XML_PRICE_ID#${3}${4}', $content);

					$maxSize = 250;
					$filesize = filesize($_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName) / 1024;

					if($filesize > $maxSize) {
						$pattern = '/<����������������>/s';
						preg_match($pattern, $content, $match);
						$offersCheck = count($match[0]) > 0;
						unset($match);

						if($offersCheck) {
							$catalogStart = '����������������';
							$catalogMiddle = '�����������';
							$catalogEnd = '�����������';
							$headerEnd = '�������';
						} else {
							$catalogStart = '�������';
							$catalogMiddle = '������';
							$catalogEnd = '�����';
							$headerEnd = '��������������';
						}

						$pattern = '/(<\?.*?<�������������>.*?<\/������������>).*?(<'.$catalogStart.'>.*?<\/����������������������>).*?<\/'.$headerEnd.'>/s';
						preg_match($pattern, $content, $match);
						if($match[0]) {
							$xmlHeader = $match[1].PHP_EOL.'	</�������������>'.PHP_EOL.'	'.$match[2].PHP_EOL.'		<'.$catalogMiddle.'>'.PHP_EOL.'			';
							$xmlFooter = PHP_EOL.'	</'.$catalogStart.'>'.PHP_EOL.'</����������������������>';
							$result = array();
							$result[1] = $match[0].$xmlFooter;

							if($result[1]) {
								file_put_contents(str_replace('.xml', '_1.xml', $_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName), $result[1]);
							}

							$needParts = ceil( ($filesize - strlen($result[1]) / 1024) / $maxSize );
							$pattern = '/<'.$catalogEnd.'>.*?<\/'.$catalogEnd.'>/s';
							preg_match_all($pattern, $content, $matches);

							if($matches[0] && $needParts) {
								$partItemsCount = ceil( count($matches[0]) / $needParts );

								for($i = 1; $i <= $needParts && $partItemsCount * ($i-1) < count($matches[0]); $i++) {
									$itemsCount = $partItemsCount * ($i-1) + $partItemsCount > count($matches[0]) ? count($matches[0]) - $partItemsCount * ($i-1) : $partItemsCount;
									$partItems = implode( PHP_EOL.'			', array_slice($matches[0], $partItemsCount * ($i-1), $itemsCount) );
									$partContent = $xmlHeader.$partItems.PHP_EOL.'		</'.$catalogMiddle.'>'.$xmlFooter;
									file_put_contents(str_replace('.xml', '_'.($i+1).'.xml', $_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName), $partContent);
								}
								if(file_exists($_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName)) {
									unlink($_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName);
								}
							}
						}
					} else {
						file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->arConfig['wizardServicesIblockExportDir'].'/'.$fileName, $content);
					}
				}
			}
		}

		return $files;
	}

	// �������� //
	private function _SetMacrosPublicReplace($file, $fileName) {

		$content = file_get_contents($file);

		if(strpos($fileName, '.menu') !== false) {

			$content = str_replace(array('"/', "'/"), array('"#SITE_DIR#', "'#SITE_DIR#"), $content);
			file_put_contents($file, $content);

			return true;

		}

		$pattern = '/[\"|\']SEF_FOLDER["|\']\s*=>\s*["|\']\//i';
		preg_match($pattern, $content, $matches);
		$need = str_replace('/', '#SITE_DIR#', $matches[0]);
		$content = str_replace($matches[0], $need, $content);
		file_put_contents($file, $content);

	}

	private function _SetMacrosPublic($dir) {

		$files = scandir($dir);

		if($files) {
			foreach ($files as $fileName) {	
				if(!is_dir($dir.'/'.$fileName)) {			
					$this->_SetMacrosPublicReplace($dir.'/'.$fileName, $fileName);
				} else {
					$this->_SetMacrosPublic($dir.'/'.$fileName);
				}
			}
		}

	}
	// �������� //

	public function _ExportIblocks(){
		foreach ($this->arIBlocks as $iblockId => $value) {
			$code = str_replace($this->arConfig['partnerID'].'_'.$this->arConfig['solutionID'].'_', '', $value['CODE']);
			$this->arIblocksForExport[] = array(
				'IBLOCK_NAME' => $value['NAME'],
				'IBLOCK_ID' => $iblockId,
				'URL_DATA_FILE' => $this->arConfig['wizardServicesIblockExportDir'].'/'.$code.'.xml',
			);
		}
	}

	private function _TemplateCreate(){
		CAsproFile::_CopyFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.$this->arConfig['templateDir'], $this->arConfig['wizardTemplateDir'], $this->arConfig['FileMasks']);
	}

	private function _PublicCreate(){

		if($this->arConfig['PUBLIC']['DIRS']) {
			foreach ($this->arConfig['PUBLIC']['DIRS'] as $dir) {
				CAsproFile::_CopyFiles($_SERVER['DOCUMENT_ROOT'].'/'.$dir, $this->arConfig['wizardSiteDir'].$dir, $this->arConfig['FileMasks']);
			}
		}

		if($this->arConfig['PUBLIC']['FILES']) {
			foreach ($this->arConfig['PUBLIC']['FILES'] as $file) {
				if(CAsproFile::_checkFileName($file, $this->arConfig['FileMasks'])) {
					@copy($_SERVER['DOCUMENT_ROOT'].'/'.$file, $this->arConfig['wizardSiteDir'].$file);
				}
			}
		}

	}

	private function _SetError($lastError = ''){
		if(strlen($lastError)){
			$this->bError = true;
			$this->lastError = $lastError;
		}

		return false;
	}

	private function _T($str = false){
		static $text;
		if($str === false || $str === null){
			$text = '';
		}
		elseif($str === true){
			return $text;
		}
		else{
			$text .= $str.PHP_EOL;
		}
	}

	private function _GetIBlockShortType($IBLOCK_ID = false){
		if(isset($this->arIBlocks[$IBLOCK_ID])){
			$type = str_replace($this->arConfig['partnerID'].'_'.$this->arConfig['solutionID'].'_', '', $this->arIBlocks[$IBLOCK_ID]['IBLOCK_TYPE_ID']);
			return $type;
		}

		return false;
	}

	private function _TranslateElementEditFormOptionsStr($fromstr){
		$tostr = '';
		$arTabs = explode(';', $fromstr);

		foreach($arTabs as $itab => $strTab){
			$arFields = explode(',-', $strTab);

			foreach($arFields as $ifield => $strField){
				$tmp = explode('#', $strField);
				$code = trim(str_replace('-', '', $tmp[0]));
				$value = trim(str_replace('-', '', $tmp[1]));

				if($isProperty = (strpos($code, 'PROPERTY_') !== false)){
					$propID = str_replace('PROPERTY_', '', $code);
					if(isset($this->arProps[$propID])){
						$code = 'PROPERTY_\'.$arProperty["'.$this->arProps[$propID]["CODE"].'"].\'';
					}
				}

				if(($valueLangID = array_search($value, $this->_arServicesIBlockLang)) === false){
					$this->_arServicesIBlockLang[] = $value;
					$this->_arServicesIBlockLang[] = '*'.$value;
					$valueLangID = array_search($value, $this->_arServicesIBlockLang);
				}

				$tostr .= ($ifield ? ',--' : '').$code.'--#--\'.GetMessage("WZD_OPTION_'.$valueLangID.'").\'--';
			}
			$tostr .= ';--';
		}

		return $tostr;
	}

	private function _TranslateElementListOptionsStr($fromstr){
		$tostr = '';
		$arFields = explode(',', $fromstr);
		foreach($arFields as $ifield => $field){
			if($isProperty = (strpos($field, 'PROPERTY_') !== false)){
				$propID = str_replace('PROPERTY_', '', $field);
				if(isset($this->arProps[$propID])){
					$field = 'PROPERTY_\'.$arProperty["'.$this->arProps[$propID]["CODE"].'"].\'';
				}
			}
			$tostr .= ($ifield ? ',' : '').$field;
		}

		return $tostr;
	}

	private function _GetElementEditFormOptionsStr($IBLOCK_ID = false){
		$str = '';
		if($IBLOCK_ID){
			$arEditElFormOp = CUserOptions::GetOption("form", "form_element_".$IBLOCK_ID);
			$str = $this->_TranslateElementEditFormOptionsStr($arEditElFormOp['tabs']);
		}

		return $str;
	}

	private function _GetIBlockPropertiesHintsUpdateStr($IBLOCK_ID = false){
		$str = '';
		if($IBLOCK_ID && $this->_arPropertiesHintsKeys[$IBLOCK_ID]){
			foreach($this->_arPropertiesHintsKeys[$IBLOCK_ID] as $CODE => $key){
				$str .= '	$ibp = new CIBlockProperty;'.PHP_EOL;
				$str .= '	$ibp->Update($arProperty["'.$CODE.'"], array("HINT" => GetMessage("'.$key.'")));'.PHP_EOL;
				$str .= '	unset($ibp);'.PHP_EOL;
			}
		}

		return $str;
	}

	private function _GetElementListOptionsStr($IBLOCK_ID = false){
		$str = '';
		if($IBLOCK_ID){
			$IBLOCK_TYPE_ID = $this->arIBlocks[$IBLOCK_ID]['IBLOCK_TYPE_ID'];
			$arListElOp = CUserOptions::GetOption("list", "tbl_iblock_list_".md5($IBLOCK_TYPE_ID.".".$IBLOCK_ID));
			$str = '\'columns\' => \''.$this->_TranslateElementListOptionsStr($arListElOp['columns']).'\', \'by\' => \''.$this->_TranslateElementListOptionsStr($arListElOp['by']).'\', \'order\' => \''.$arListElOp['order'].'\', \'page_size\' => \''.$arListElOp['page_size'].'\'';
		}

		return $str;
	}

	private function _GenerateWizardServicesIBlockLang(){
		if($this->bError){
			return false;
		}

		if($this->_arServicesIBlockLang){
			$this->_T();
			$this->_T('<?');
			foreach($this->_arServicesIBlockLang as $ilang => $langValue){
				$langValue = str_replace('"', '\'', $langValue);
				$this->_T('$MESS["WZD_OPTION_'.$ilang.'"] = "'.$langValue.'";');
			}
			$this->_T('?>');
			$text = $this->_T(true);
			$filePath = $this->arConfig['wizardServicesDir'].'iblock/lang/ru/editform_useroptions.php';
			$r = @file_put_contents($filePath, $text);
			if($r === false){
				return $this->_SetError('Error generate file '.$filePath);
			}
			@copy($filePath, str_replace('/ru/', '/en/', $filePath));
		}

		if($this->_arPropertiesHints){
			$this->_T();
			$this->_T('<?');
			foreach($this->_arPropertiesHints as $key => $value){
				$value = str_replace('\"', '\'', $value);
				$this->_T('$MESS["'.$key.'"] = "'.$value.'";');
			}
			$this->_T('?>');
			$text = $this->_T(true);
			$filePath = $this->arConfig['wizardServicesDir'].'iblock/lang/ru/properties_hints.php';
			$r = @file_put_contents($filePath, $text);
			if($r === false){
				return $this->_SetError('Error generate file '.$filePath);
			}
			@copy($filePath, str_replace('/ru/', '/en/', $filePath));
		}

		return true;
	}

	private function _GenerateWizardServicesIBlockAllScripts(){
		if($this->bError){
			return false;
		}

		$arFormsShortCODE = array();
		$arFormsShortCODEstr = '';
		$lastFormTypeIBLOCK_ID = false;

		foreach($this->arIBlocks as $IBLOCK_ID => $arIBlock){
			if(in_array($IBLOCK_ID, $this->arConfig['IBlockIDs'])){
				$tmp = str_replace($this->arConfig['partnerID'].'_'.$this->arConfig['solutionID'].'_', '', $this->arIBlocks[$IBLOCK_ID]['CODE']);
				$fileName = $IBlockShortCode = $tmp;
				if(!$isFormType = ($IBlockShortType = $this->_GetIBlockShortType($IBLOCK_ID)) == 'form'){
					if(!$this->_GenerateWizardServicesIBlockScript($IBLOCK_ID, $fileName, $IBlockShortType)){
						return false;
					}
				}
				else{
					$arFormsShortCODE[] = $IBlockShortCode;
					$lastFormTypeIBLOCK_ID = $IBLOCK_ID;
				}
			}
		}

		if($arFormsShortCODE){
			$arFormsShortCODEstr = '$arFormsShortCODE = array(';
			foreach($arFormsShortCODE as $i =>$shortCODE){
				$arFormsShortCODEstr .= ($i ? ', ' : '').'"'.$shortCODE.'"';
			}
			$arFormsShortCODEstr .= ');';
			if(!$this->_GenerateWizardServicesIBlockScript($lastFormTypeIBLOCK_ID, 'forms', 'form', true, $arFormsShortCODEstr)){
				return false;
			}
		}

		return true;
	}

	private function _GenerateWizardServicesIBlockScript($IBLOCK_ID = false, $fileName = '', $IBlockShortType = '', $isFormType = false, $arFormsShortCODEstr = ''){
		if(isset($this->arIBlocks[$IBLOCK_ID]) || $isFormType){
			$IBlockShortCode = $fileName;
			if(strlen($fileName)){
				$this->_T();
				$this->_T('<?');
				$this->_T('if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();');
				$this->_T('if(!CModule::IncludeModule("iblock")) return;');
				$this->_T('');
				$this->_T('if(!defined("WIZARD_SITE_ID")) return;');
				$this->_T('if(!defined("WIZARD_SITE_DIR")) return;');
				$this->_T('if(!defined("WIZARD_SITE_PATH")) return;');
				$this->_T('if(!defined("WIZARD_TEMPLATE_ID")) return;');
				$this->_T('if(!defined("WIZARD_TEMPLATE_ABSOLUTE_PATH")) return;');
				$this->_T('if(!defined("WIZARD_THEME_ID")) return;');
				$this->_T('');
				$this->_T('$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."/";');
				$this->_T('//$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"]."/local/templates/".WIZARD_TEMPLATE_ID."/";');
				$this->_T('');
				if($isFormType){
					$this->_T($arFormsShortCODEstr);
					$this->_T('foreach($arFormsShortCODE as $iblockShortCODE){');
				}
				else{
					$this->_T('$iblockShortCODE = "'.$fileName.'";');
				}
				$this->_T('$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/".$iblockShortCODE.".xml";');
				$this->_T('$iblockTYPE = "'.$this->arConfig['partnerID'].'_'.$this->arConfig['solutionID'].'_'.$IBlockShortType.'";');
				$this->_T('$iblockXMLID = "'.$this->arConfig['partnerID'].'_'.$this->arConfig['solutionID'].'_".$iblockShortCODE."_".WIZARD_SITE_ID;');
				$this->_T('$iblockCODE = "'.$this->arConfig['partnerID'].'_'.$this->arConfig['solutionID'].'_".$iblockShortCODE;');
				$this->_T('$iblockID = false;');
				$this->_T('');
				$this->_T('$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockXMLID, "TYPE" => $iblockTYPE));');
				$this->_T('if ($arIBlock = $rsIBlock->Fetch()) {');
				$this->_T('	$iblockID = $arIBlock["ID"];');
				$this->_T('	if (WIZARD_INSTALL_DEMO_DATA) {');
				$this->_T('		// delete if already exist & need install demo');
				$this->_T('		CIBlock::Delete($arIBlock["ID"]);');
				$this->_T('		$iblockID = false;');
				$this->_T('	}');
				$this->_T('}');
				$this->_T('');
				$this->_T('if(WIZARD_INSTALL_DEMO_DATA){');
				$this->_T('	if(!$iblockID){');
				$this->_T('		// add new iblock');
				$this->_T('		$permissions = array("1" => "X", "2" => "R");');
				$this->_T('		$dbGroup = CGroup::GetList($by = "", $order = "", array("STRING_ID" => "content_editor"));');
				$this->_T('		if($arGroup = $dbGroup->Fetch()){');
				$this->_T('			$permissions[$arGroup["ID"]] = "W";');
				$this->_T('		};');
				$this->_T('		');
				$this->_T('		// replace macros IN_XML_SITE_ID & IN_XML_SITE_DIR in xml file - for correct url links to site');
				$this->_T('		if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back")){');
				$this->_T('			@copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back", $_SERVER["DOCUMENT_ROOT"].$iblockXMLFile);');
				$this->_T('		}');
				$this->_T('		@copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, $_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back");');
				$this->_T('		CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, Array("IN_XML_SITE_DIR" => WIZARD_SITE_DIR));');
				$this->_T('		CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, Array("IN_XML_SITE_ID" => WIZARD_SITE_ID));');
				$this->_T('		$iblockID = WizardServices::ImportIBlockFromXML($iblockXMLFile, $iblockCODE, $iblockTYPE, WIZARD_SITE_ID, $permissions);');
				$this->_T('		if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back")){');
				$this->_T('			@copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile.".back", $_SERVER["DOCUMENT_ROOT"].$iblockXMLFile);');
				$this->_T('		}');
				$this->_T('		if ($iblockID < 1)	return;');
				$this->_T('			');
				$this->_T('		// iblock fields');
				$this->_T('		$iblock = new CIBlock;');
				$this->_T('		$arFields = array(');
				$this->_T('			"ACTIVE" => "Y",');
				$this->_T('			"CODE" => $iblockCODE,');
				$this->_T('			"XML_ID" => $iblockXMLID,');
				$this->_T('			"FIELDS" => array(');
				$this->_T('				"IBLOCK_SECTION" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['IBLOCK_SECTION']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['IBLOCK_SECTION']['DEFAULT_VALUE'].'",');
				$this->_T('				),');
				$this->_T('				"ACTIVE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['ACTIVE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE"=> "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['ACTIVE']['DEFAULT_VALUE'].'",');
				$this->_T('				),');
				$this->_T('				"ACTIVE_FROM" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['ACTIVE_FROM']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['ACTIVE_FROM']['DEFAULT_VALUE'].'",');
				$this->_T('				),');
				$this->_T('				"ACTIVE_TO" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['ACTIVE_TO']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['ACTIVE_TO']['DEFAULT_VALUE'].'",');
				$this->_T('				),');
				$this->_T('				"SORT" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SORT']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SORT']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"NAME" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['NAME']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['NAME']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"PREVIEW_PICTURE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => array(');
				$this->_T('						"FROM_DETAIL" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['FROM_DETAIL'].'",');
				$this->_T('						"SCALE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['SCALE'].'",');
				$this->_T('						"WIDTH" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['WIDTH'].'",');
				$this->_T('						"HEIGHT" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['HEIGHT'].'",');
				$this->_T('						"IGNORE_ERRORS" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['IGNORE_ERRORS'].'",');
				$this->_T('						"METHOD" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['METHOD'].'",');
				if($this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['COMPRESSION']) {
					$this->_T('						"COMPRESSION" => '.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['COMPRESSION'].',');
				}
				$this->_T('						"DELETE_WITH_DETAIL" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['DELETE_WITH_DETAIL'].'",');
				$this->_T('						"UPDATE_WITH_DETAIL" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_PICTURE']['DEFAULT_VALUE']['UPDATE_WITH_DETAIL'].'",');
				$this->_T('					),');
				$this->_T('				), ');
				$this->_T('				"PREVIEW_TEXT_TYPE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_TEXT_TYPE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_TEXT_TYPE']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"PREVIEW_TEXT" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_TEXT']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['PREVIEW_TEXT']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"DETAIL_PICTURE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => array(');
				$this->_T('						"SCALE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['DEFAULT_VALUE']['SCALE'].'",');
				$this->_T('						"WIDTH" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['DEFAULT_VALUE']['WIDTH'].'",');
				$this->_T('						"HEIGHT" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['DEFAULT_VALUE']['HEIGHT'].'",');
				$this->_T('						"IGNORE_ERRORS" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['DEFAULT_VALUE']['IGNORE_ERRORS'].'",');
				$this->_T('						"METHOD" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['DEFAULT_VALUE']['METHOD'].'",');
				$this->_T('						"COMPRESSION" => '.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_PICTURE']['DEFAULT_VALUE']['COMPRESSION'].',');
				$this->_T('					),');
				$this->_T('				), ');
				$this->_T('				"DETAIL_TEXT_TYPE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_TEXT_TYPE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_TEXT_TYPE']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"DETAIL_TEXT" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_TEXT']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['DETAIL_TEXT']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"XML_ID" =>  array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['XML_ID']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['XML_ID']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"CODE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => array(');
				$this->_T('						"UNIQUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['UNIQUE'].'",');
				$this->_T('						"TRANSLITERATION" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['TRANSLITERATION'].'",');
				$this->_T('						"TRANS_LEN" => '.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['TRANS_LEN'].',');
				$this->_T('						"TRANS_CASE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['TRANS_CASE'].'",');
				$this->_T('						"TRANS_SPACE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['TRANS_SPACE'].'",');
				$this->_T('						"TRANS_OTHER" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['TRANS_OTHER'].'",');
				$this->_T('						"TRANS_EAT" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['TRANS_EAT'].'",');
				$this->_T('						"USE_GOOGLE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['CODE']['DEFAULT_VALUE']['USE_GOOGLE'].'",');
				$this->_T('					),');
				$this->_T('				),');
				$this->_T('				"TAGS" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['TAGS']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['TAGS']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"SECTION_NAME" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_NAME']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_NAME']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"SECTION_PICTURE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => array(');
				$this->_T('						"FROM_DETAIL" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['FROM_DETAIL'].'",');
				$this->_T('						"SCALE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['SCALE'].'",');
				$this->_T('						"WIDTH" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['WIDTH'].'",');
				$this->_T('						"HEIGHT" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['HEIGHT'].'",');
				$this->_T('						"IGNORE_ERRORS" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['IGNORE_ERRORS'].'",');
				$this->_T('						"METHOD" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['METHOD'].'",');
				$this->_T('						"COMPRESSION" => '.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['COMPRESSION'].',');
				$this->_T('						"DELETE_WITH_DETAIL" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['DELETE_WITH_DETAIL'].'",');
				$this->_T('						"UPDATE_WITH_DETAIL" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_PICTURE']['DEFAULT_VALUE']['UPDATE_WITH_DETAIL'].'",');
				$this->_T('					),');
				$this->_T('				), ');
				$this->_T('				"SECTION_DESCRIPTION_TYPE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DESCRIPTION_TYPE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DESCRIPTION_TYPE']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"SECTION_DESCRIPTION" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DESCRIPTION']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DESCRIPTION']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"SECTION_DETAIL_PICTURE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => array(');
				$this->_T('						"SCALE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['DEFAULT_VALUE']['SCALE'].'",');
				$this->_T('						"WIDTH" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['DEFAULT_VALUE']['WIDTH'].'",');
				$this->_T('						"HEIGHT" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['DEFAULT_VALUE']['HEIGHT'].'",');
				$this->_T('						"IGNORE_ERRORS" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['DEFAULT_VALUE']['IGNORE_ERRORS'].'",');
				$this->_T('						"METHOD" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['DEFAULT_VALUE']['METHOD'].'",');
				$this->_T('						"COMPRESSION" => '.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_DETAIL_PICTURE']['DEFAULT_VALUE']['COMPRESSION'].',');
				$this->_T('					),');
				$this->_T('				), ');
				$this->_T('				"SECTION_XML_ID" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_XML_ID']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_XML_ID']['DEFAULT_VALUE'].'",');
				$this->_T('				), ');
				$this->_T('				"SECTION_CODE" => array(');
				$this->_T('					"IS_REQUIRED" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['IS_REQUIRED'].'",');
				$this->_T('					"DEFAULT_VALUE" => array(');
				$this->_T('						"UNIQUE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['UNIQUE'].'",');
				$this->_T('						"TRANSLITERATION" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['TRANSLITERATION'].'",');
				$this->_T('						"TRANS_LEN" => '.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['TRANS_LEN'].',');
				$this->_T('						"TRANS_CASE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['TRANS_CASE'].'",');
				$this->_T('						"TRANS_SPACE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['TRANS_SPACE'].'",');
				$this->_T('						"TRANS_OTHER" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['TRANS_OTHER'].'",');
				$this->_T('						"TRANS_EAT" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['TRANS_EAT'].'",');
				$this->_T('						"USE_GOOGLE" => "'.$this->arIBlocks[$IBLOCK_ID]['FIELDS']['SECTION_CODE']['DEFAULT_VALUE']['USE_GOOGLE'].'",');
				$this->_T('					),');
				$this->_T('				), ');
				$this->_T('			),');
				$this->_T('		);');
				$this->_T('		');
				$this->_T('		$iblock->Update($iblockID, $arFields);');
				$this->_T('	}');
				$this->_T('	else{');
				$this->_T('		// attach iblock to site');
				$this->_T('		$arSites = array(); ');
				$this->_T('		$db_res = CIBlock::GetSite($iblockID);');
				$this->_T('		while ($res = $db_res->Fetch())');
				$this->_T('			$arSites[] = $res["LID"]; ');
				$this->_T('		if (!in_array(WIZARD_SITE_ID, $arSites)){');
				$this->_T('			$arSites[] = WIZARD_SITE_ID;');
				$this->_T('			$iblock = new CIBlock;');
				$this->_T('			$iblock->Update($iblockID, array("LID" => $arSites));');
				$this->_T('		}');
				$this->_T('	}');
				$this->_T('');
				if(!$isFormType){
					$this->_T('	// iblock user fields');
					$this->_T('	$dbSite = CSite::GetByID(WIZARD_SITE_ID);');
					$this->_T('	if($arSite = $dbSite -> Fetch()) $lang = $arSite["LANGUAGE_ID"];');
					$this->_T('	if(!strlen($lang)) $lang = "ru";');
					$this->_T('	WizardServices::IncludeServiceLang("editform_useroptions.php", $lang);');
					$this->_T('	WizardServices::IncludeServiceLang("properties_hints.php", $lang);');
					$this->_T('	$arProperty = array();');
					$this->_T('	$dbProperty = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $iblockID));');
					$this->_T('	while($arProp = $dbProperty->Fetch())');
					$this->_T('		$arProperty[$arProp["CODE"]] = $arProp["ID"];');
					$this->_T('');
					$this->_T('	// properties hints');
					$this->_T($this->_GetIBlockPropertiesHintsUpdateStr($IBLOCK_ID));
					$this->_T('	// edit form user options');
					$ElementEditFormOptionsStr = $this->_GetElementEditFormOptionsStr($IBLOCK_ID);
					$this->_T('	CUserOptions::SetOption("form", "form_element_".$iblockID, array(');
					$this->_T('		"tabs" => \''.$ElementEditFormOptionsStr.'\',');
					$this->_T('	));');
				}
				$this->_T('	// list user options');
				if($isFormType){
					$this->_T('CUserOptions::SetOption("list", "tbl_iblock_list_".md5($iblockTYPE.".".$iblockID), array(');
					$this->_T('	"columns" => "NAME,ACTIVE,SORT,TIMESTAMP_X,ID", "by" => "date_active_from", "order" => "desc", "page_size" => "20", ');
					$this->_T('));');
				}
				else{
					$ElementListOptionsStr = $this->_GetElementListOptionsStr($IBLOCK_ID);
					$this->_T('	CUserOptions::SetOption("list", "tbl_iblock_list_".md5($iblockTYPE.".".$iblockID), array(');
					$this->_T('		'.$ElementListOptionsStr.',');
					$this->_T('	));');
				}
				$this->_T('}');
				$this->_T('');
				$this->_T('if($iblockID){');
				$this->_T('	// replace macros IBLOCK_TYPE & IBLOCK_ID & IBLOCK_CODE');
				if($isFormType){
					$this->_T('CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("IBLOCK_FORM_".strtoupper($iblockShortCODE)."_TYPE" => $iblockTYPE));');
					$this->_T('CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("IBLOCK_FORM_".strtoupper($iblockShortCODE)."_ID" => $iblockID));');
					$this->_T('CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("IBLOCK_FORM_".strtoupper($iblockShortCODE)."_CODE" => $iblockCODE));');
					$this->_T('CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("IBLOCK_FORM_".strtoupper($iblockShortCODE)."_TYPE" => $iblockTYPE));');
					$this->_T('CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("IBLOCK_FORM_".strtoupper($iblockShortCODE)."_ID" => $iblockID));');
					$this->_T('CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("IBLOCK_FORM_".strtoupper($iblockShortCODE)."_CODE" => $iblockCODE));');
				}
				else{
					$this->_T('	CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("IBLOCK_'.strtoupper($IBlockShortCode).'_TYPE" => $iblockTYPE));');
					$this->_T('	CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("IBLOCK_'.strtoupper($IBlockShortCode).'_ID" => $iblockID));');
					$this->_T('	CWizardUtil::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("IBLOCK_'.strtoupper($IBlockShortCode).'_CODE" => $iblockCODE));');
					$this->_T('	CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("IBLOCK_'.strtoupper($IBlockShortCode).'_TYPE" => $iblockTYPE));');
					$this->_T('	CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("IBLOCK_'.strtoupper($IBlockShortCode).'_ID" => $iblockID));');
					$this->_T('	CWizardUtil::ReplaceMacrosRecursive($bitrixTemplateDir, Array("IBLOCK_'.strtoupper($IBlockShortCode).'_CODE" => $iblockCODE));');
				}
				$this->_T('}');
				if($isFormType){
					$this->_T('}');
				}
				$this->_T('?>');
				$text = $this->_T(true);

				$filePath = $this->arConfig['wizardServicesDir'].'iblock/'.$fileName.'.php';
				$r = @file_put_contents($filePath, $text);
				if($r === false){
					return $this->_SetError('Error generate file '.$filePath);
				}
			}
		}

		return true;
	}
}
?>

<script>

function StartExport(params, key){
	var queryString = 'Export=Y&lang=ru&<?echo bitrix_sessid_get()?>&INTERVAL=30';
	queryString += '&URL_DATA_FILE='+jsUtils.urlencode(params[key].URL_DATA_FILE);
	queryString += '&IBLOCK_ID='+params[key].IBLOCK_ID;
	queryString += '&SECTIONS_FILTER=active&ELEMENTS_FILTER=active';

	if(typeof($) == 'function') {
		var informer = $('#stepInformer');
		if(informer.length) {
			informer.find('.stepInfo').text((key+1)+' �� '+params.length+' '+params[key].IBLOCK_NAME+' ('+params[key].IBLOCK_ID+')');
		}
	}

	BX.ajax.post(
		'/bitrix/admin/iblock_xml_export.php?'+queryString,
		{},
		function(result){
			$('#exportContainer').html(result);
		}
	);
}

function DoNext(NS)
{
	var interval = 30;
	var queryString =
		'Export=Y'
		+ '&lang=<?=LANGUAGE_ID?>'
		+ '&<?echo bitrix_sessid_get()?>'
		+ '&INTERVAL=' + interval
	;

	if(!NS)
	{
		queryString+='&URL_DATA_FILE='+jsUtils.urlencode(document.getElementById('URL_DATA_FILE').value);
		queryString+='&IBLOCK_ID='+jsUtils.urlencode(document.getElementById('IBLOCK_ID').value);
		queryString+='&SECTIONS_FILTER='+jsUtils.urlencode(document.getElementById('SECTIONS_FILTER').value);
		queryString+='&ELEMENTS_FILTER='+jsUtils.urlencode(document.getElementById('ELEMENTS_FILTER').value);
		if(document.getElementById('CK_DOWNLOAD_CLOUD_FILES'))
			queryString+='&DOWNLOAD_CLOUD_FILES='+(document.getElementById('CK_DOWNLOAD_CLOUD_FILES').checked? 'Y': 'N');
	}

	BX.ajax.post(
		'/bitrix/admin/iblock_xml_export.php?'+queryString,
		NS,
		function(result){
			$('#exportContainer').html(result);
		}
	);
}

function EndExport() {
	arIblocksForExportKey++;
	if(arIblocksForExportKey in arIblocksForExport) {
		setTimeout(function(){StartExport(arIblocksForExport, arIblocksForExportKey)}, 500);
	} else {
		BX.ajax.post(
			window.location.href,
			{'AUTO_WIZ_LAST_ACTIONS': true},
			function(){
				if(typeof($) == 'function') {
					var informer = $('#stepInformer');
					if(informer.length) {
						informer.find('.stepName').text('��������� ���������. ');
						informer.find('.stepInfo').text('�������������� ���������� - '+arIblocksForExportKey);
					}
				}
			}
		);
	}
}

function SetLinks(){
	var data = {};
	data.SetLinks = 'Y';
	BX.ajax.post(
		window.location.href,
		data,
		function(result){
			
		}
	);
}

</script>