$(document).ready(function(){

	setInterval(saveOptions, 5000);

	$(document).on('click', ".nextStep:not(.disabled)", function() {

		var currStep = $(".stepWrapper.active");
		currStep.slideUp(800);

		var nextStep = $(".stepWrapper.active").next().next();
		if(nextStep.hasClass('stepWrapper')) {

			nextStep.slideDown(800, function(){
				$(this).addClass('active');
			});

		}

		if(!nextStep.next().next().hasClass('stepWrapper')) {
			$(".nextStep").addClass('disabled');
			$(".nextStep").attr('disabled', '');
		}
		
		currStep.removeClass("active");
		$(".prevStep").removeClass('disabled');
		$(".prevStep").removeAttr('disabled');

		saveOptions();

	});

	$(document).on('click', ".prevStep:not(.disabled)", function() {

		var currStep = $(".stepWrapper.active");
		currStep.slideUp(800);

		var prevStep = $(".stepWrapper.active").prev().prev();
		if(prevStep.hasClass('stepWrapper')) {

			prevStep.slideDown(800, function(){
				$(this).addClass('active');
			});
			
		}

		if(!prevStep.prev().prev().hasClass('stepWrapper')) {
			$(".prevStep").addClass('disabled');
			$(".prevStep").attr('disabled', '');
		}
		
		currStep.removeClass("active");
		$(".nextStep").removeClass('disabled');
		$(".nextStep").removeAttr('disabled');

		saveOptions();

	});

	$('#formAutoWizard #submitAutoWizard').click(function() {
		saveOptions();

		postData = $('#formAutoWizard input').serialize();
		postData += '&AUTO_WIZ_FIRST_ACTIONS=true';
		
		BX.ajax.post(
			window.location.href,
			postData
		);

		if(typeof($) == 'function') {
			var informer = $('#stepInformer');
			if(informer.length) {
				informer.find('.stepName').text('Экспорт инфоблоков: ');
				informer.show();
			}
		}
	});

	function saveOptions(){
		$url = window.location.href.split('?')[0]+'SaveOptions.php';
		BX.ajax.post(
			$url,
			$('#formAutoWizard input').serialize()
		);
	}
	

});