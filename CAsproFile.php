<?

class CAsproFile {

    public static function _checkFileName($fileName, $masks){
		foreach ($masks as $mask) {
			$mask = str_replace(array('.', '(', ')', '/', '$', '?', '^'), array('\.', '\(', '\)', '\/', '\$', '\?', '\^'), $mask);
			$mask = str_replace('*', '.*', $mask);
			$mask = '/^'.$mask.'$/';
			if(preg_match($mask, $fileName)) {return false;}
		}

		return true;
    }

    public static function _FindFiles($dirFrom, $dirTo, $masks){
		$files = scandir($dirFrom);
		
		if($files) {
			foreach ($files as $fileName) {
				if(!in_array($fileName, array('.', '..'))) {
					
					if(is_dir($dirFrom.'/'.$fileName)) {
						self::_CopyFiles($dirFrom.'/'.$fileName, $dirTo.'/'.$fileName, $masks);
					} else if(self::_checkFileName($fileName, $masks)){
						$result[] = $fileName;
					}
				}
			}
		}

		return $result;
	}

	public static function _CopyFiles($dirFrom, $dirTo, $masks){
		if (is_dir($dirFrom)) {
			if(!is_dir($dirTo) && !mkdir($dirTo, 0777, true)){
				return false;
			} else {
				if($files = self::_FindFiles($dirFrom, $dirTo, $masks)){
					foreach ($files as $fileName) {
						@copy($dirFrom.'/'.$fileName, $dirTo.'/'.$fileName);
					}
				}
			}
		} else {
			return false;
		}
	}
    
}

?>