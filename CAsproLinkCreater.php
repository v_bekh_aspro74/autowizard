<?

class CAsproLinkCreater {

	private $props = array();
	private $cacheClass = '';
	private $moduleId = '';
	private $iblockId = '';

	function __construct($iblocks, $cacheClass, $moduleId) {

		$this->iblockId = $iblocks;
		$this->cacheClass = $cacheClass;
		$this->moduleId = $moduleId;
		$this->moduleCode = str_replace('.', '_', $moduleId).'_';
		$this->moduleName = explode('.', $moduleId)[1];
		$this->iblocks = array();
		$this->elements = array();
		$this->props = array();
		$this->arEmptyProps = array();
		$this->linkElements = array();
		$this->arFinalOrder = '';

		$this->propsInfo = array();
		$this->sectionsInfo = array();
		$this->newPropSections = array();
		$this->newPropIblocks = array();
		$this->newPropProps = array();
		$this->newPropElementsInfo = array();

		$this->result = '';

	}

    public function create() {

        if(CModule::IncludeModule('iblock')) {

    		$this->analysisIblocks();
    		$this->getLinkElements();
        	$this->result = $this->generateLinks();
			$this->result['preview_text'] = $this->setPreviewText();
			$this->result['new_prop_text'] = self::newPropCreateResultText();
			$this->result['iblocks_text'] = $this->setIblocksValues($this->result['iblockNames']);
			$this->result['iblock_elements_text'] = $this->setIblockElementsValues($this->result['iblockElements']);

			$this->result['final_text'] = $this->arFinalOrder.$this->result['preview_text'].PHP_EOL.PHP_EOL.$this->result['iblocks_text'].PHP_EOL.PHP_EOL.$this->result['iblock_elements_text'].PHP_EOL.PHP_EOL.$this->result['links_text'].PHP_EOL.$this->result['new_prop_text'].$this->result['custom_text'].'?>';
		    

            return $this->result['final_text'];
        }

    }


    public function analysisIblocks() {

    	if($this->iblockId) {

    		$iblocksIds = array_keys($this->iblockId);

			$resProps = CIBlockProperty::GetList( array(),array('PROPERTY_TYPE' => 'E') );
			while($prop = $resProps->Fetch()) {
				if(in_array($prop['IBLOCK_ID'], $iblocksIds)) {
					$this->props[$prop['IBLOCK_ID']][] = $prop;
				}
			}

			foreach($iblocksIds as $iblockId){
				if( !isset($this->props[$iblockId]) ) {
					$arFinalOrder[] = $iblockId;
				}
			}

			foreach($this->props as $iblockId => $arProps_) {
				$add = true;
				foreach($arProps_ as $keyProp => $prop) {
					if(!in_array( $prop['LINK_IBLOCK_ID'], $arFinalOrder )) {
						$add = false;
						$arNeedIblocks[$prop['LINK_IBLOCK_ID']] = $prop['LINK_IBLOCK_ID'];
					} else {
						unset($this->props[$iblockId][$keyProp]);
					}
				}

				if($add) {
					$arFinalOrder[] = $iblockId;
					unset($this->props[$iblockId]);
				}
			}

			$arFilter = array('LOGIC' => 'OR');
			$arGroup = array('IBLOCK_ID');

			foreach($this->props as $iblockId => $arProps_) {
				if( in_array($iblockId, $arNeedIblocks) ) {
					foreach($arProps_ as $keyProp => $prop) {
						$arFilter[] = array('IBLOCK_ID' => $iblockId, '!PROPERTY_'.$prop['CODE'] => false);
					}
				}
			}


			$resElements = CIBlockElement::GetList( array("CNT"=>"ASC"), $arFilter, $arGroup );

			while($row = $resElements->Fetch()) {
				$arFinalOrder[] = $row['IBLOCK_ID'];
				foreach($this->props[$row['IBLOCK_ID']] as $keyProp => $prop) {
					if( !in_array($prop['LINK_IBLOCK_ID'], $arFinalOrder) || $prop['LINK_IBLOCK_ID'] == $row['IBLOCK_ID'] ) {
						$this->arEmptyProps[] = $prop;
					}
				}

				unset($this->props[$row['IBLOCK_ID']]);
			}

			foreach($this->props as $iblockId => $prop) {
				$arFinalOrder[] = $iblockId;
			}

			$this->arFinalOrder = '"STAGES" => Array('.PHP_EOL;
			foreach($arFinalOrder as $iblockId) {
				$this->arFinalOrder .= '	"'.str_replace($this->moduleCode, '', self::getIblockVal($iblockId, 'IBLOCK_CODE') ).'.php"'.PHP_EOL;
			}
			$this->arFinalOrder .= '),'.PHP_EOL;

    	}


    }


    public function getIblockVal($iblockId, $val = '') {

    	if(!isset($this->iblocks[$iblockId])) {

    		$iblockFetched = CIBlock::GetByID($iblockId)->Fetch();

    		$iblockCode = $iblockFetched['CODE'];
			$iblockCodeShort = str_replace($this->moduleCode, '', $iblockCode);

			$this->iblocks[$iblockId] = array(
				'ID' => $iblockId,
				'ELEMENT_VAR' => '$ar'.ucfirst($iblockCodeShort),
				'IBLOCK_VAR' => '$'.$iblockCodeShort.'IBlockID',
				'IBLOCK_CODE' => $iblockCode,
				'XML_ID' => $iblockFetched['XML_ID'],
				'TYPE' => $iblockFetched['IBLOCK_TYPE_ID'],
			);

		}

		if($val && isset($this->iblocks[$iblockId][$val])) {
			return $this->iblocks[$iblockId][$val];
		} else {
			return $this->iblocks[$iblockId];
		}

    }

    public function getPropVal($propId, $val = '') {

    	if(!isset($this->propsInfo[$propId])) {

    		$propXmlId = CIBlockProperty::GetByID($propId)->Fetch()['XML_ID'];

			$this->propsInfo[$propId] = array(
				'ID' => $propId,
				'XML_ID' => $propXmlId,
			);

		}

		if($val && isset($this->propsInfo[$propId][$val])) {
			return $this->propsInfo[$propId][$val];
		} else {
			return $this->propsInfo[$propId];
		}

    }

    public function getSectionkVal($sectionId, $val = '') {

    	if(!isset($this->sectionsInfo[$sectionId])) {

    		$sectionXmlId = CIBlockSection::GetByID($sectionId)->Fetch()['XML_ID'];

			$this->sectionsInfo[$sectionId] = array(
				'ID' => $sectionId,
				'XML_ID' => $sectionXmlId,
			);

		}

		if($val && isset($this->sectionsInfo[$sectionId][$val])) {
			return $this->sectionsInfo[$sectionId][$val];
		} else {
			return $this->sectionsInfo[$sectionId];
		}

    }

    public function getElementInfo($elementId) {

    	if(!isset($this->elements[$elementId])) {

    		$res = CIBlockElement::GetByID($elementId);
			if($ar_res = $res->Fetch()) {
			  $this->elements[$elementId]['IBLOCK_ID'] = $ar_res['IBLOCK_ID'];
			  $this->elements[$elementId]['XML_ID'] = $ar_res['XML_ID'];
			}

		}

		return $this->elements[$elementId];

    }


    public function generateLinks() {

		foreach($this->linkElements as $linkElement){

			$counter = 1;
			foreach($linkElement['LINK_PROPS'] as $propKey => $propInfo){

				$arLinkItemsStr .= '"'.$propKey.'" => array(';

				foreach ($propInfo['VALUE'] as $propValueKey => $propValue) {
					if(!in_array($propInfo['IBLOCK']['IBLOCK_CODE'], $result['iblockNames'])) {
						$result['iblockNames'][] = $propInfo['IBLOCK']['IBLOCK_CODE'];
					}

					if(!in_array($propInfo['IBLOCK']['ID'], $result['iblockElements'])) {
						$result['iblockElements'][$propInfo['IBLOCK']['ID']] = $propInfo['IBLOCK']['ID'];
					}

					$arLinkItemStrTmp = '';
					if($propValueKey != count($propInfo['VALUE'])-1){
						$arLinkItemStrTmp .= $propInfo['IBLOCK']['ELEMENT_VAR'].'["'.$propValue.'"], ';
					} else {
						$arLinkItemStrTmp .= $propInfo['IBLOCK']['ELEMENT_VAR'].'["'.$propValue.'"]';
					}
					$arLinkItemsStr .= $arLinkItemStrTmp;
				}

				if($counter == count($linkElement['LINK_PROPS'])){
					$arLinkItemsStr .= ')';
				} else{
					$arLinkItemsStr .= '), ';
				}

				$counter++;
			}



			$iblockCode = next($this->linkElements)['IBLOCK']['IBLOCK_CODE'];
			$strUpdateNew = '// update links in '.$iblockCode;

			if(!$strUpdateOld){
				$result['links_text'] .= PHP_EOL.$strUpdateNew.PHP_EOL;
			}

			$result['links_text'] .= 'CIBlockElement::SetPropertyValuesEx('.$linkElement['IBLOCK']['ELEMENT_VAR'].'["'.$linkElement['XML_ID'].'"], '.$linkElement['IBLOCK']['IBLOCK_VAR'].', array('.$arLinkItemsStr.'));'.PHP_EOL;

			if($strUpdateNew != $strUpdateOld && $strUpdateOld && $iblockCode){
				$result['links_text'] .= PHP_EOL.$strUpdateNew.PHP_EOL;
			}

			$arLinkItemsStr = '';

			$strUpdateOld = $strUpdateNew;

			if(!in_array($linkElement['IBLOCK']['IBLOCK_CODE'], $result['iblockNames'])) {
				$result['iblockNames'][] = $linkElement['IBLOCK']['IBLOCK_CODE'];
			}

			if(!in_array($linkElement['IBLOCK']['ID'], $result['iblockElements'])) {
				$result['iblockElements'][] = $linkElement['IBLOCK']['ID'];
			}

		}

		return $result;

    }


    public function setIblocksValues($iblockNames) {

    	$result = '// iblocks ids'.PHP_EOL;

		$iblocksRes = CIBlock::GetList(array("SORT"=>"ASC"),array('CODE' => $iblockNames));
		while($iblock = $iblocksRes->Fetch()) {
    		$iblocks[$iblock['ID']] = array(
    			'VAR' => $this->getIblockVal($iblock['ID'], 'IBLOCK_VAR'),
    			'CODE' => $iblock['CODE'],
    			'TYPE' => $iblock['IBLOCK_TYPE_ID'],
    		); 
		}

    	foreach ($iblocks as $iblock) {
    		$result .= $iblock['VAR'].' = '.$this->cacheClass.'::$arIBlocks[WIZARD_SITE_ID]["'.$iblock['TYPE'].'"]["'.$iblock['CODE'].'"][0];'.PHP_EOL;
    	}
        
        return $result;
    }

    public function setIblockElementsValues($iblockIds) {

    	$result = '// elements ids'.PHP_EOL;

    	foreach ($iblockIds as $iblock) {
			$result .= $this->getIblockVal($iblock, 'ELEMENT_VAR').' = '.$this->cacheClass.'::CIBlockElement_GetList(array("CACHE" => array("TIME" => 0, "TAG" => '.$this->cacheClass.'::GetIBlockCacheTag('.$this->getIblockVal($iblock, 'IBLOCK_VAR').'), "GROUP" => array("XML_ID"), "RESULT" => array("ID"))), array("IBLOCK_ID" => '.$this->getIblockVal($iblock, 'IBLOCK_VAR').'), false, false, array("ID", "XML_ID"));'.PHP_EOL;
		}
		
        return $result;
    }

    public function setPreviewText() {
    	$result = '<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();'.PHP_EOL;
    	$result .= 'if(!CModule::IncludeModule("iblock")) return;'.PHP_EOL;
    	$result .= 'if(!CModule::IncludeModule("'.$this->moduleId.'")) return;'.PHP_EOL.PHP_EOL;

    	$result .= 'if(!defined("WIZARD_SITE_ID")) return;'.PHP_EOL;
    	$result .= 'if(!defined("WIZARD_SITE_DIR")) return;'.PHP_EOL;
    	$result .= 'if(!defined("WIZARD_SITE_PATH")) return;'.PHP_EOL;
    	$result .= 'if(!defined("WIZARD_TEMPLATE_ID")) return;'.PHP_EOL;
    	$result .= 'if(!defined("WIZARD_TEMPLATE_ABSOLUTE_PATH")) return;'.PHP_EOL;
    	$result .= 'if(!defined("WIZARD_THEME_ID")) return;'.PHP_EOL.PHP_EOL;

    	$result .= '$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."/";'.PHP_EOL;

		return $result;

    }

    public function getLinkElements() {

		$arSelectLink = array('ID', 'XML_ID', 'IBLOCK_ID');
		$arFilterLink['LOGIC'] = 'OR';
		foreach($this->arEmptyProps as $prop){
			$arFilterLink[] = array(
				'!PROPERTY_'.$prop['CODE'] => false,
				'IBLOCK_ID' => $prop['IBLOCK_ID'],
			);

			if(!in_array('PROPERTY_'.$prop['CODE'], $arSelectLink)) {
				$arSelectLink[] = 'PROPERTY_'.$prop['CODE'];
				$arSelectLink[] = 'PROPERTY_'.$prop['CODE'].'.IBLOCK_ID';
				$arSelectLink[] = 'PROPERTY_'.$prop['CODE'].'.XML_ID';
			}

		}

		$resEl = CIBlockElement::GetList(array("IBLOCK_ID"=>"ASC"), $arFilterLink, false, false, $arSelectLink);
		while($el = $resEl->Fetch()){

			$el = $this->prepareLinkElement($el);

			if(isset($this->linkElements[$el['ID']])) {
				if($el['LINK_PROPS']) {

					foreach ($el['LINK_PROPS'] as $linkKey => $linkValue) {
						if(!in_array($linkValue['VALUE'][0], $this->linkElements[$el['ID']]['LINK_PROPS'][$linkKey]['VALUE'])) {
							$this->linkElements[$el['ID']]['LINK_PROPS'][$linkKey]['VALUE'][] = $linkValue['VALUE'][0];
						}
					}

				}
			} else {
				$elementIblock = $this->getIblockVal($el['IBLOCK_ID']);

				$this->linkElements[$el['ID']] = $el;
				$this->linkElements[$el['ID']]['IBLOCK'] = $elementIblock;
			}


		}

		return $this->linkElements;
		

    }

    public function getProps() {

    	$arFilter = array('PROPERTY_TYPE' => 'E');
        $resProps = CIBlockProperty::GetList(array(),$arFilter);

        while ($arProp = $resProps->fetch()) {
        	if(in_array($arProp['ID'], $this->propsId)) {
        		$this->props[] = array(
        			'ID' => $arProp['ID'],
        			'CODE' =>$arProp['CODE'],
        			'IBLOCK_ID' =>$arProp['IBLOCK_ID'],
        		);
        	}
		}

    }

    public function prepareLinkElement($element) {

		foreach ($element as $key => $value) {

			// fix elements where no xml_id or iblock_id after getList
			if(strpos($key, '_VALUE') !== false && $value && strpos($key, '_VALUE_ID') === false) {
				$iblockPropKey = str_replace('_VALUE', '_IBLOCK_ID', $key);
				$xmlPropKey = str_replace('_VALUE', '_XML_ID', $key);
				if(!$element[$iblockPropKey] || !$element[$xmlPropKey]) {
					$iblockProp = $this->getElementInfo($value)['IBLOCK_ID'];
					$xmlProp = $this->getElementInfo($value)['XML_ID'];
					if($this->getIblockVal($iblockProp) && $xmlProp) {
						$propKey = str_replace(array('PROPERTY_', '_IBLOCK_ID', '_XML_ID'), '', $key);
						$result['LINK_PROPS'][$propKey] = array(
							'IBLOCK' => $this->getIblockVal($iblockProp),
							'VALUE' => array($xmlProp),
						);
					}
				}

			}

			if(strpos($key, '_VALUE') === false && $value){

				if(strpos($key, 'PROPERTY_') !== false) {
					$propKey = str_replace(array('PROPERTY_', '_IBLOCK_ID', '_XML_ID'), '', $key);
					if($element['PROPERTY_'.$propKey.'_XML_ID'] && $this->getIblockVal($element['PROPERTY_'.$propKey.'_IBLOCK_ID'])) {

						$result['LINK_PROPS'][$propKey] = array(
							'IBLOCK' => $this->getIblockVal($element['PROPERTY_'.$propKey.'_IBLOCK_ID']),
							'VALUE' => array($element['PROPERTY_'.$propKey.'_XML_ID']),
						);
					}
				} else {
					$result[$key] = $value;
				}

			}

		}

		return $result;

    }


    public function newPropGetProps() {
    	$arFilter = array('PROPERTY_TYPE' => 'S', 'USER_TYPE' => 'SAsproCustomFilter'.$this->moduleName, 'ACTIVE' => 'Y');

		$resProps = CIBlockProperty::GetList(
		 	array(),
			$arFilter
		);

		while ($prop = $resProps->Fetch() ) {
			$arProps[$prop['ID']] = array(
				'ID' => $prop['ID'],
				'CODE' => $prop['CODE'],
				'IBLOCK_ID' => $prop['IBLOCK_ID'],
				'XML_ID' => $prop['XML_ID'],
			);
		}

		return $arProps;
    }

    public function newPropGetElements() {
    	$arElementFilter = array('LOGIC' => 'OR');
		$arElementSelect = array('ID', 'CODE', 'IBLOCK_ID', 'XML_ID');

		$arProps = self::newPropGetProps();

		foreach ($arProps as $prop) {
			$arElementFilter[] = array(
				'LOGIC' => 'AND',
				array('IBLOCK_ID' => $prop['IBLOCK_ID'], 'ACTIVE' => 'Y', '!PROPERTY_'.$prop['CODE'] => false),
				array('IBLOCK_ID' => $prop['IBLOCK_ID'], 'ACTIVE' => 'Y', '!PROPERTY_'.$prop['CODE'] => '[]'),
			);
			$arElementSelect[] = 'PROPERTY_'.$prop['CODE'];
		}

		$resElements = CIBlockElement::GetList(
			 array("SORT"=>"ASC"),
			 $arElementFilter,
			 false,
			 false,
			 $arElementSelect
		);

		while ($element = $resElements->Fetch() ) {
			$arElements[$element['ID']] = $element;
		}

		foreach($arElements as $keyElement => $element) {
			foreach($element as $key => $elementProp) {
				if(!$elementProp || strpos($key, 'VALUE_ID') !== false) {
					unset($arElements[$keyElement][$key]);
				} else if(strpos($key, '_VALUE') !== false) {
					$decoded = json_decode($elementProp, true);
					if($decoded !== NULL && $decoded['CHILDREN']) {
						$arElements[$keyElement][$key] = self::newPropChildrenAnalizer($decoded);

						$this->newPropElementsInfo[$element['ID']]['INFO'] = array(
							'ID' => $element['ID'],
							'XML_ID' => $element['XML_ID'],
							'IBLOCK_ID' => $element['IBLOCK_ID'],
						);

						$this->newPropElementsInfo[$element['ID']]['PROP_CODE'][] = $key;
						$this->newPropElementsInfo[$element['ID']]['PROP_STR'][] = $arElements[$keyElement][$key];

					}
				}
			}
		}

		//$this->result['custom_text'] .= var_export($this->newPropElementsInfo, true);
    }

    public function newPropChildrenAnalizer($arCondition) {
		if($arCondition['CHILDREN']) {
			foreach($arCondition['CHILDREN'] as $condKey => $condition) {
				if($condition['CLASS_ID'] == 'CondIBSection') {
					$this->newPropSections[$condition['DATA']['value']] = self::getSectionkVal($condition['DATA']['value'], 'XML_ID');
					$arCondition['CHILDREN'][$condKey]['DATA']['value'] = '\'.$arNewPropSections["'.self::getSectionkVal($condition['DATA']['value'], 'XML_ID').'"].\'';
				} else if($condition['CLASS_ID'] == 'CondGroup' && $condition['CHILDREN']) {
					$arCondition['CHILDREN'][$condKey] = self::newPropChildrenAnalizer($arCondition['CHILDREN'][$condKey]);
				} else if(strpos($condition['CLASS_ID'], 'CondIBProp') === 0) {
					$exploded = explode(':', $condition['CLASS_ID']);

					$this->newPropIblocks[$exploded[1]] = $exploded[1];
					$this->newPropProps[$exploded[2]] = self::getPropVal($exploded[2], 'XML_ID');

					$iblock = self::getIblockVal($exploded[1], 'IBLOCK_VAR');

					$arClassId = array(
						'CondIBProp',
						'\'.'.$iblock.'.\'',
						'\'.$arNewPropProps["'.self::getPropVal($exploded[2], 'XML_ID').'"].\'',
					);

					$macros = '#'.count($this->newPropDataValues).'#';

					$arCondition['CHILDREN'][$condKey]['CLASS_ID'] = implode(':', $arClassId);
					$arCondition['CHILDREN'][$condKey]['DATA']['value'] = $macros;

					$this->newPropDataValues[$macros] = $condition['DATA']['value'];
				}
			}
		}

		return json_encode($arCondition);
	}

	public function newPropCreateResultText() {
		$result = '';
		self::newPropGetElements();

		if($this->newPropElementsInfo) {
			$result .= PHP_EOL;

			$result .= '// get sections'.PHP_EOL;
			$result .= '$newPropSectionsXML = '.var_export($this->newPropSections, true).';'.PHP_EOL;
			$result .= '$newPropSectionsRes = CIBlockSection::GetList(array(), array("XML_ID" => $newPropSectionsXML, "IBLOCK_ID" => $catalogIBlockID), false, array("ID", "XML_ID"));'.PHP_EOL;
			$result .= 'while($newPropSection = $newPropSectionsRes->Fetch()) {'.PHP_EOL;
			$result .= '	$arNewPropSections[$newPropSection["XML_ID"]] = $newPropSection["ID"];'.PHP_EOL;
			$result .= '}'.PHP_EOL;


			$result .= PHP_EOL.'// get props'.PHP_EOL;
			$result .= '$newPropPropsXML = '.var_export($this->newPropProps, true).';'.PHP_EOL;

			$result .= 'foreach($newPropPropsXML as $xml) {'.PHP_EOL;
			$result .= '	$resNewProps = CIBlockProperty::GetList('.PHP_EOL;
			$result .= '		array(),'.PHP_EOL;
			$result .= '		array("XML_ID" => $xml, "IBLOCK_ID" => $catalogIBlockID)'.PHP_EOL;
			$result .= '	);'.PHP_EOL;
			$result .= '	$newProp = $resNewProps->Fetch();'.PHP_EOL;
			$result .= '	$arNewPropProps[$newProp["XML_ID"]] = $newProp["ID"];'.PHP_EOL;
			$result .= '}'.PHP_EOL;


			$result .= PHP_EOL.'// update values custom filter'.PHP_EOL;
			foreach ($this->newPropElementsInfo as $elementKey => $element) {

				$this->result['iblockNames'][] = self::getIblockVal($element['INFO']['IBLOCK_ID'], 'IBLOCK_CODE');
				$this->result['iblockElements'][self::getIblockVal($element['INFO']['IBLOCK_ID'], 'ID')] = self::getIblockVal($element['INFO']['IBLOCK_ID'], 'ID');

				$result .= 'CIBlockElement::SetPropertyValuesEx('.self::getIblockVal($element['INFO']['IBLOCK_ID'], 'ELEMENT_VAR').'["'.$element['INFO']['XML_ID'].'"], ';
				$result .= self::getIblockVal($element['INFO']['IBLOCK_ID'], 'IBLOCK_VAR').', array(';
				foreach($element['PROP_CODE'] as $propKey => $propValue){
					$result .= '"'.str_replace(array('PROPERTY_', '_VALUE'), '', $propValue).'" => ';

					$element['PROP_STR'][$propKey] = '\''.str_replace('\\', '', $element['PROP_STR'][$propKey]).'\'';
					if($this->newPropDataValues) {
						foreach ($this->newPropDataValues as $macrosKey => $macros) {
							$element['PROP_STR'][$propKey] = str_replace($macrosKey, '\'.iconv(LANG_CHARSET, "UTF-8", "'.$macros.'").\'', $element['PROP_STR'][$propKey]);
						}
					}
					$result .= $element['PROP_STR'][$propKey];
					
					if($propKey < count($element['PROP_CODE']) - 1) {
						$result .= ', ';
					}
				}
				$result .= '));'.PHP_EOL;

			}

		}

		return $result;
	}


}

?>